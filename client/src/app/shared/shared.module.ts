import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

// i18n

// Third-Party Modules
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { FlexLayoutModule } from '@angular/flex-layout';

const THIRDMODULES = [
    FlexLayoutModule,
    NgZorroAntdModule,
];

const DIRECTIVES = [];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        // third libs
        ...THIRDMODULES
    ],
    declarations: [
        ...DIRECTIVES
    ],
    exports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        // third libs
        ...THIRDMODULES,
        ...DIRECTIVES,
    ],
})
export class SharedModule {}