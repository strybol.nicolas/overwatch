import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared.module';

// Components
import {
    SignInComponent,
    SignUpComponent
} from './components';

const COMPONENTS = [
    SignInComponent,
    SignUpComponent
];

@NgModule({
    declarations: [
        ...COMPONENTS
    ],
    imports: [
        CommonModule,
        SharedModule
    ],
    exports: [
        ...COMPONENTS
    ],
})
export class HeaderModule {}