import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { AuthService } from '../../../../core/services';

@Component({
	selector: 'app-sign-up',
	templateUrl: './sign-up.component.html',
	styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {


	@Output() isSignUpVisibleChange = new EventEmitter<boolean>();

	@Input()
    set setIsSignUpVisible(isSignUpVisible: boolean) {
        this.isSignUpVisible = isSignUpVisible;
    }
	
	isSignUpVisible: boolean = false;

	constructor(
        private _authService: AuthService
    ) { }

	ngOnInit() {
	}

	// Modal functions
	onSignUp() {
		console.log("Sign Up - onSignUp");
        this._authService.signOut();
		this.isSignUpVisible = !this.isSignUpVisible;
		this.isSignUpVisibleChange.emit(this.isSignUpVisible);
	}

	onCancel() {
		console.log("Sign Up - onCancel");

		this.isSignUpVisible = !this.isSignUpVisible;
		this.isSignUpVisibleChange.emit(this.isSignUpVisible);
	}

}
