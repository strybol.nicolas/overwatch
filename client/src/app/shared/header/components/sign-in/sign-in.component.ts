import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

import { AuthService } from '../../../../core/services';

@Component({
	selector: 'app-sign-in',
	templateUrl: './sign-in.component.html',
	styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {


    @Output() isSignInVisibleChange = new EventEmitter<Boolean>();
    @Output() isSignUpVisibleChange = new EventEmitter<Boolean>();

	@Input()
    set setIsSignInVisible(isSignInVisible: boolean) {
        this.isSignInVisible = isSignInVisible;
    }
	
    isSignInVisible: Boolean = false;

    isDisabled: Boolean = false;

    credentials = {
        username: "",
        password: ""
    };

    form: FormGroup;

	constructor(
        private _authService: AuthService,
        private fb: FormBuilder
    ) { 

        this.init();
    }

	ngOnInit() { }

	// Modal functions
	onSignIn() {
        console.log("Sign In - OnSignIn");
        this.isDisabled = true;


        let isAuthenticated = this._authService
            .authenticate(
                this.form.get("username").value, 
                this.form.get("password").value);

        if( isAuthenticated ) {

            this.init();

            this.isSignInVisible = !this.isSignInVisible;
    	    this.isSignInVisibleChange.emit(this.isSignInVisible);
        } else {
            
            console.log("AUTHENTICATED FAILED!");
        }

	}

	onCancel() {
        console.log("Sign In - onCancel");
        
        this.init();

		this.isSignInVisible = !this.isSignInVisible;
		this.isSignInVisibleChange.emit(this.isSignInVisible);
    }
    
    onClick() {
        console.log("onClick()");
        this.isDisabled = !this.isDisabled;
    }

    newUser() {
        
        this.isSignUpVisibleChange.emit(true);

        this.isSignInVisible = !this.isSignInVisible;
    	this.isSignInVisibleChange.emit(this.isSignInVisible);
    }

    init() {

        this.isDisabled = false;

        this.form = this.fb.group({
            username: new FormControl("", Validators.required),
            password: new FormControl("", Validators.required)
        });
    }

}

