import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';

import { AuthService } from '../../core/services';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

    @Input() isCollapsed: boolean;
    @Output() isCollapsedChange = new EventEmitter<boolean>();
    
    isSignInVisible: boolean = false;
    isSignUpVisible: boolean = false;

    constructor(
        private _authService: AuthService
    ) { }

    ngOnInit() {
    }


    setIsCollapsed() {
        this.isCollapsed = !this.isCollapsed;

        this.isCollapsedChange.emit(this.isCollapsed);
    }

    /** Sign In **/

    /**
     * 
     */
    signIn() {
        console.log("Sign In - Click");
        this.isSignInVisible = true;
    }

    /**
     * 
     * @param isVisible: boolean
     */
    isSignInVisibleChange(isSignInVisible: boolean) {
        this.isSignInVisible = isSignInVisible;
    }

    /** Sign up **/

    /**
     * 
     */
    signUp() {
        console.log("Sign Up - Click");

        this.isSignUpVisible = true;
    }

    /**
     * 
     * @param isVisible: boolean
     */
    isSignUpVisibleChange(isSignUpVisible: boolean) {
        this.isSignUpVisible = isSignUpVisible;
    }

    /**
     * 
     */
    signOut() {
        console.log("Sign Out - Click");

        this._authService.signOut();
    }
}
