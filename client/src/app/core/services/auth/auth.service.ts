import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    adminCredentials = {
        username: "admin",
        password: "*Override"
    }

    constructor() { }

    authenticate(username, password) {
        if ( username === this.adminCredentials.username && 
             password === this.adminCredentials.password ) {

            sessionStorage.setItem('username', username);
            return true;
        } else {
            return false;
        }
    }

    isUserSignedIn() {

        let user = sessionStorage.getItem('username')

        //console.log(!(user === null));
        return user === null;
    }

    signOut() {

        sessionStorage.removeItem('username')
    }
}
