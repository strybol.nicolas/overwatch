import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { NzNotificationService } from 'ng-zorro-antd/notification';

import { ApiService } from '../api/api.service';

@Injectable({
    providedIn: 'root'
})
export class ConfigsService {

    url: string = "configs";
    configs$: Observable<any>;

    tempResult = [];
    constructor(
        private apiService: ApiService,
        private nzNotification: NzNotificationService
    ) { }

    

    

    /** HTTP Requests **/

    getAll(): Observable<any> {
        console.log("---- ConfigsService ( getAll ) ----");
        return this.apiService.getAllRequest(this.url);
    }

    update(configs): void {
        console.log("---- ConfigsService ( save ) ----");
        this.apiService.putRequest(this.url, configs.id, configs).subscribe(
            (res) => {
                console.log("---- PUT Success ----");
                this.nzNotification.success(
                    "Success", 
                    "Successfully updated!");
            },
            (err) => {
                console.log("---- PUT Error ----");
                this.nzNotification.error(
                    "Error", 
                    "Something went wrong!");
            }
        );
    }


    /** Formatters **/
    toTreeSelect(data): any {
        console.log("---- toTreeSelect ----");
        console.log("data =");
        console.log(data);

        // TODO
        
        return [];
    }
}
