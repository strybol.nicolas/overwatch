import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ApiService {

    apiUrl = "//localhost:8080/api/";

    constructor(
        private http: HttpClient
    ) { }

    /**
     * 
     * @param url 
     */
    getAllRequest(url: string): Observable<any> {
        console.log("---- HTTP GET ----");
        return this.http.get(this.apiUrl + url);
    }

    /**
     * 
     * @param url 
     * @param object 
     */
    postRequest(url: string, object) {
        console.log("---- HTTP POST ----");
        return this.http.post(this.apiUrl + url + "/", object);
    }

    /**
     * 
     * @param url (string)
     * @param id (string)
     * @param object 
     */
    putRequest(url: string, id: string, object) {
        console.log("---- HTTP PUT ----");
        return this.http.put(this.apiUrl + url + "/" + id, object);
    }

    /**
     * 
     * @param url (string)
     * @param id (string) 
     */
    deleteRequest(url: string, id: string) {
        console.log("---- HTTP DELETE ----");
        console.log(this.apiUrl + url + "/" + id);
        return this.http.delete(this.apiUrl + url + "/" + id);
    }

}
