import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { NzNotificationService } from 'ng-zorro-antd/notification';

import { ApiService } from '../api/api.service';

@Injectable({
    providedIn: 'root'
})
export class MembersService {

    url: string = "members";
    members$: Observable<any>;

    constructor(
        private apiService: ApiService,        
        private nzNotification: NzNotificationService
    ) { }

    getAll(): Observable<any> {
        console.log("---- MembersService ( getAll ) ----");
        return this.apiService.getAllRequest(this.url);
    }

    insert(member) {
        console.log("---- MembersService ( insert ) ----");
        console.log(member);
        this.apiService.postRequest(this.url, member).subscribe(
            (res) => {
                console.log("---- POST Success ----");
                this.nzNotification.success(
                    "Success", 
                    "Successfully created!");
            },
            (err) => {
                console.log("---- POST Error ----");
                this.nzNotification.error(
                    "Error", 
                    "Something went wrong!");
            }
        );
    }

    update(member) {
        console.log("---- MembersService ( update ) ----");
        this.apiService.putRequest(this.url, member.id, member).subscribe(
            (res) => {
                console.log("---- PUT Success ----");
                this.nzNotification.success(
                    "Success", 
                    "Successfully updated!");
            },
            (err) => {
                console.log("---- PUT Error ----");
                this.nzNotification.error(
                    "Error", 
                    "Something went wrong!");
            }
        );
    }

    delete(member) {
        console.log("---- MembersService ( delete ) ----");
        this.apiService.deleteRequest(this.url, member.id).subscribe(
            (res) => {
                console.log("---- DELETE Success ----");
                this.nzNotification.success(
                    "Success", 
                    "Successfully deleted!");
            },
            (err) => {
                console.log("---- DELETE Error ----");
                this.nzNotification.error(
                    "Error", 
                    "Something went wrong!");
            }
        );
    }
}
