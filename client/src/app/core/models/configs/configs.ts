export class Configs {

    _id: string;
    categories: Array<object>;
    ranks: {
        starbase: object
    };
    roles: {
        starbase: object
    }
}
