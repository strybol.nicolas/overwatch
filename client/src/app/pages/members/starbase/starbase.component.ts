import { Component, OnInit } from '@angular/core';

import { ConfigsService, MembersService } from 'src/app/core/services';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-members-starbase',
    templateUrl: './starbase.component.html',
    styleUrls: ['./starbase.component.scss']
})
export class StarbaseComponent implements OnInit {

    starbase$: Observable<any>;
    filteredMembers;

    constructor(
        private _configsService: ConfigsService,
        private _membersService: MembersService
    ) { }


    ngOnInit() { this.fetchConfigs(); }


    /** Init Methods **/

    /**
     * 
     */
    fetchConfigs() {
        this._configsService.getAll().subscribe(
            (configs) => {
            console.log("---- _configsService ----");

            console.log("ranksData = ");
            console.log(configs[0].ranks);
            console.log("rolesData = ");
            console.log(configs[0].roles);
            
            this.prepTabs(configs[0].ranks, configs[0].roles);
        });
    }


    /**
     * 
     * @param ranks 
     * @param roles 
     */
    prepTabs(ranks, roles) {
        console.log("---- prepTabs (tabset) ----");

        this.filteredMembers = [{ 
            tab: "All", 
            configs: { 
                ranks: ranks, 
                roles: roles 
            },
            list: [] 
        }];
        
        for( let i = 0; i < ranks[0].children.length; i++ ) {

            this.filteredMembers.push({ 
                tab: this.getFormattedTitle(ranks[0].children[i].title), 
                configs: { 
                    ranks: ranks, 
                    roles: roles 
                }, 
                list: [] 
            });
        }

        this.fetchMembers();
    }

    /**
     * 
     */
    fetchMembers() {

        this._membersService.getAll().subscribe(
            (starbase) => {
                console.log("---- _membersService ----");
                console.log(starbase);

                this.filteredMembers[0].list = starbase;

                this.getStarbaseMembersFiltered();
            }
        );
    }


    /**
     * 
     */
    getStarbaseMembersFiltered() {
        console.log("---- getStarbaseMembers ----");
        console.log("this.filteredMembers =");
        console.log(this.filteredMembers);

        for( let i = 1; i < this.filteredMembers.length; i++ ) {
            
            this.filteredMembers[i].list = this.filteredMembers[0].list.filter(
                element => this.filterByRole(element, this.filteredMembers[0].configs.ranks[0].children[i-1].title)
            );
        }
    }


    /** onChange Methods **/

    /**
     * Depending on state, shall call the API for CRUD operations
     * @param data 
     */
    onChange(data) {
        console.log("---- onChange (" + data.state + ") ----");
        console.log(data);
        
        switch(data.state) {
            case "INSERT":
                console.log("---- Calling memberService.insert ----");
                this._membersService.insert(data.data);
                break;
            case "UPDATE": 
                console.log("---- Calling memberService.update ----");
                this._membersService.update(data.data);
                break;
            case "DELETE":
                console.log("---- Calling memberService.delete ----");
                this._membersService.delete(data.data);
                this.fetchConfigs();
                break;
            case "REFRESH":
                console.log("---- Calling init() ----");
                this.filteredMembers = null;
                this.fetchConfigs();
                break;
            default:
                console.log("onChange() default!");
                break;
        }
    }


    /** Filter & sort **/

    /**
     * 
     * @param element
     * @param role (string)
     */
    filterByRole(element, role: string): boolean {
        console.log("---- filterByRole ----");
        console.log(element);
        console.log(role);
        if( element != null ) {
            return element.roles.indexOf(role) != -1;
        }
    }

    /** Sort alphabetically */

    /**
     * 
     * @param first 
     * @param second
     */
    sortAplhabetically(first, second): number {
        return (first.toUpperCase() < second.toUpperCase()) ? -1 : (first.toUpperCase() > second.toUpperCase()) ? 1 : 0;
    }

    /** Formatting **/

    /**
     * Will take property name and format it to be more suitable for users' eyes
     * @param title (string)
     */
    getFormattedTitle(title: string): string {
        let temp = title.split(/(?=[A-Z])/);

        return temp[0][0].toUpperCase() + temp[0].substr(1);
    }
}
