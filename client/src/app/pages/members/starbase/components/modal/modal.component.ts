import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { ConfigsService } from '@core/services/configs'

@Component({
    selector: 'app-modal',
    templateUrl: './modal.component.html',
    styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

    @Output() isVisibleChange = new EventEmitter<Object>();

    @Input()
    set setData(data) { this.init(data); }

    treeNodeRegex = /\w+\-\w+\-\w+/;
    treeNodeRegex_bis = /\w+\-\w+/;

    treeSelectRanks = [];
    treeSelectRoles = [];

    ranksValues: string[] = [];             // TODO: Restrict ranks can only be 1 per faction/server
    rolesValues: string[] = [];             // For now, user vigilence is expected

    isVisible: boolean = false;
    title: String = "";

    member;
    temp;

    constructor(
        private configsService: ConfigsService
    ) { }

    ngOnInit() { }


    /**
     * Handles received data and prepares modal for creating/editing Member
     * 
     * @param data
     */
    init(data) {
        console.log("---- init() Modal ----");
        this.isVisible = data.isVisible;
        this.temp = data.configs.ranks;
        this.setTreeNodes(data.configs);

        if( this.isEmpty(data.member) ) {
            console.log("---- Opening Modal (Insert) ----");
            this.title = "Insert";
            this.member = {
                discordInfo: {
                    id: "",
                    username: "",
                    tag: ""
                }
            };
        } else {
            console.log("---- Opening Modal (Update) ----");
            this.title = "Update";
            this.member = data.member;
        }
        
    }

    /**
     * Sets Ranks & Roles, in TreeNode structure, for TreeSelect
     * 
     * @param configs 
     */
    setTreeNodes(configs) {
        console.log("---- setTreeNodes ----");
        this.treeSelectRanks = configs.ranks;
        this.treeSelectRoles = configs.roles;
    }

    /**
     * Executed when user clicks on 'Save'
     */
    onSave() {
        console.log("---- onSave ----");
        this.closeModal(true);
    }

    /**
     * Executed when user clicks on 'Cancel'
     */
    onCancel() {
        console.log("---- onCancel ----");
        this.closeModal(false);
    }

    /**
     * Handle & prepare request to be sent to calling component
     */
    closeModal(toSave: boolean) {
        console.log("---- closeModal() ----");
        let request = {
            isVisible: false,
            operation: {
                name: this.title.toUpperCase(),
                toSave: toSave,
            },
            member: this.member
        }
        console.log(request);
        this.isVisible = false;

        this.isVisibleChange.emit(request);
    }


    /** TreeSelect onChange() methods **/

    /**
     * 
     * @param event 
     */
    onRanksChange(event) {
        
        console.log(event);
        console.log(this.ranksValues);
        this.ranksValues = this.ranksValues.filter(
            (el) => {
                
                if(
                    el == 'starbase-diplomats' || 
                    el == 'starbase-republic'
                ) {
                    return this.treeNodeRegex_bis.test(el)
                } 

                return this.treeNodeRegex.test(el)
            }
        );

        let result = this.configsService.formatUserTreeNodes(this.temp, this.ranksValues);
        console.log(result);
        if( this.member.ranks ) {
            
            Object.keys(result).forEach(element => {
                this.member.ranks[element] = result[element];
            });  
        } else {
            
            this.member["ranks"] = result;
        }
        console.log(this.member);
    }

    /**
     * 
     * @param event 
     */
    onRolesChange(event) {
        
        this.rolesValues = this.rolesValues.filter(
            (el) => {
                
                if(
                    el == 'starbase-diplomats' || 
                    el == 'starbase-republic'
                ) {
                    return this.treeNodeRegex_bis.test(el)
                } 

                return this.treeNodeRegex.test(el)
            }
        );
        
        let result = this.configsService.formatUserTreeNodes(this.temp, this.rolesValues);
        
        if( this.member.roles ) {
            
            Object.keys(result).forEach(element => {
                this.member.roles[element] = result[element];
            });  
        } else {
            
            this.member["roles"] = result;
        }
    }


    /**
     * Returns true if obj is empty, else false
     * Used to determine if modal is used to create a new Member or edit an existing Member
     * 
     * @param obj (object)
     * @returns boolean
     */
    isEmpty(obj): boolean {

        for( var key in obj ) {
            if( obj.hasOwnProperty(key) ) {

                return false;
            }
        }

        return true;
    }
    
}
