import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

import { ConfigsService } from '@core/services';

@Component({
    selector: 'app-starbase-table',
    templateUrl: './table.component.html',
    styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

    @Output() onChange = new EventEmitter<any>();

    @Input() 
    set received(value: Object) {
        this._data = value || {};
    }
    get received(): Object {
        return this._data;
    }

    /** Info to send to modal **/
    modal = {
        isVisible: false,
        configs: {},
        member: {}
    };

    /** Info to send to drawer **/
    drawer = {
        isVisible: false,
        member: {}
    };

    tags = { 
        games: [], 
        groups: [],
        roles: []
    };

    _data;
    
    loading: boolean = true;

    constructor(
        private configsService: ConfigsService
    ) { }

    ngOnInit() { }

    /** Role tags **/

    /**
     * 
     * @param roles (Array<string>)
     * @param toIgnore (string)
     */
    prepTags(elements) {     
        console.log("---- prepTags ----");
        console.log(elements);
        if(elements) {
            console.log(Object.keys(elements));
            return Object.keys(elements);
        }
    }

    

    /** Actions **/

    /**
     * Opens modal to create a new member
     * @param item 
     */
    insert() {
        console.log("---- insert() ----");
        this.modal = {
            isVisible: true,
            configs: this._data.configs,
            member: {}
        };
    }

    /**
     * Opens drawer to view member's information
     * @param item 
     */
    view(item) {
        console.log("---- view() ----");
        this.drawer = {
            isVisible: true,
            member: item
        };
    }

    /**
     * Opens modal to edit selected member
     * 
     * @param item 
     */
    update(item) {
        console.log("---- update() ----");
        this.modal = {
            isVisible: true,
            configs: this._data.configs,
            member: item
        };
    }

    /**
     * 
     * @param item 
     */
    delete(item) {
        console.log("---- delete() ----");
        this.onChange.emit({ 
            state:"DELETE", 
            data: item
        });
    }

    /**
     * 
     */
    refresh() {
        console.log("---- refresh() ----");
        this.onChange.emit({ 
            state:"REFRESH"
        });
    }

    /** Popconfirm Methods **/

    /**
     * 
     * @param item 
     */
    onConfirm(item) {
        console.log("Delete confirmed!");
        this.delete(item);
    }

    /**
     * 
     * @param item 
     */
    onCancel(item) {
        console.log("Delete cancelled!");
    }

    /** Filters **/

    searchByUsername(subString: string) {
        console.log("---- pre ----");
        console.log(this._data);
        this._data.list = this._data.list.filter(
            (el) => {
                if(el.discordInfo.username.indexOf(subString) >= 0) {
                    return true;
                } else {
                    return false;
                }
            }
        );
        console.log("---- post ----");
        console.log(this._data);
    }

    reset() {
        console.log("---- pre ----");
        console.log(this.received);
        this._data = this.received;
        console.log("---- post ----");
        console.log(this._data);
    }

    /** Formatters **/

    getShortUsername(member): string {
        let limitChar = 12;
        let result = "";
        //console.log(member);
        if( member.discordInfo.username.length > (limitChar + 3) ) {
            //console.log("username too big!")
            result = member.discordInfo.username.substring(0, limitChar);
            result += "...";
        } else {
            //console.log("Username not too big!");
            result = member.discordInfo.username;
        }

        return result;
    }

    /**
     * 
     * @param string 
     */
    capitalize(string: string) {
        console.log("---- capitalize ----");
        console.log(string);
        if( string != null ) {
            
            let result = string.charAt(0).toUpperCase() + string.slice(1);
            return result;
        }
        
    }

    /**
     * 
     * @param string 
     */
    addSpaces(string: string) {
        console.log("---- addSpaces ----");
        console.log(string);
        if( string != null ) {
            return string.split(/(?=[A-Z])/).join(" ");
        }       
    }
    /** Emitters **/

    /**
     * 
     * @param isVisible: boolean
     */
    isDrawerVisibleChange(isVisible: boolean) {
        this.drawer.isVisible = isVisible;
    }

    /**
     * 
     * @param isVisible: boolean
     */
    isModalVisibleChange(data) {
        this.modal.isVisible = data.isVisible;

        if( data.operation.toSave ) {
            this.onChange.emit({ 
                state: data.operation.name, 
                data: data.member
            });
        }
    }

}
