import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-drawer-view',
    templateUrl: './drawer.component.html',
    styleUrls: ['./drawer.component.scss']
})
export class DrawerComponent implements OnInit {

    @Output() isVisibleChange = new EventEmitter<Boolean>();

	@Input()
    set init(data) {
        console.log("---- Opening drawer ----");
        this.isVisible = data.isVisible;
        this.member = data.member;
    }

    member;
    isVisible: Boolean = false;
    
    constructor() { }

    ngOnInit() { }

    onClose() {
        this.isVisible = false;
        this.isVisibleChange.emit(this.isVisible);
    }
}
