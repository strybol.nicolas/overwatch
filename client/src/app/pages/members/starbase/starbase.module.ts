import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../shared/shared.module';

import { 
    DrawerComponent,
    ModalComponent,
    TableComponent    
} from './components';


const COMPONENTS = [
    DrawerComponent,
    ModalComponent,
    TableComponent    
];

@NgModule({
    declarations: [
        ...COMPONENTS,
    ],
    imports: [
        CommonModule,
        SharedModule
    ],
    exports: [
        ...COMPONENTS
    ],
})
export class StarbaseModule {}