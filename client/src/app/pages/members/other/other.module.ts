import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../shared/shared.module';

import { 
    TableComponent,
} from './components';


const COMPONENTS = [
    TableComponent
];

@NgModule({
    declarations: [
        ...COMPONENTS,
    ],
    imports: [
        CommonModule,
        SharedModule
    ],
    exports: [
        ...COMPONENTS
    ],
})
export class OtherModule {}