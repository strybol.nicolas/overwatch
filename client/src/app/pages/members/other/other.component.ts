import { Component, OnInit } from '@angular/core';

import { MembersService } from '../../../core/services'

@Component({
    selector: 'app-members-other',
    templateUrl: './other.component.html',
    styleUrls: ['./other.component.scss']
})
export class OtherComponent implements OnInit {

    filteredMembers = [];

    constructor(
        private _membersService: MembersService
    ) { }

    ngOnInit() {

        this.prepTabs();
        this.fetchMembers();
    }


    /** Init Methods **/

    /** */
    prepTabs() {

        let array = ["All", "CS:GO", "Empyrion", "League", "Minecraft"];

        for (let el of array) {

            this.filteredMembers.push({ tab: el, list: [] });
        }
    }

    /** */
    fetchMembers() {

        this._membersService.getAll().subscribe(data => {
            console.log("---- subscribe ----");
            console.log(data);
            let members$ = data;

            members$.forEach(
                item => {
                    let roles: Array<string> = item.roles;

                    roles.sort(
                        (a, b) => {
                            let textA = a.toUpperCase();
                            let textB = b.toUpperCase();
                            return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
                        }
                    );

                    item.roles = roles;
                }
            )

            this.filterMembers(members$);
        });
    }


    /** Filter **/

    /**
     * 
     * @param members 
     */
    filterMembers(members) {

        for (let i = 0; i < this.filteredMembers.length; i++) {

            if (this.filteredMembers[i].tab != "All") {

                this.filteredMembers[i].list = members.filter(
                    element => this.filterByGame(element, this.filteredMembers[i].tab)
                );
            } else {

                this.filteredMembers[i].list = members;
            }
        }
        console.log("---- this.filteredMembers ----");
        console.log(this.filteredMembers);
    }

    /**
     * 
     * @param element 
     * @param game 
     */
    filterByGame(element, game: string): boolean {
        return element.roles.indexOf(game) != -1;
    }

}
