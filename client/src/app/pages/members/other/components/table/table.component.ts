import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-other-table',
    templateUrl: './table.component.html',
    styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

    @Input() item;

    limitTags: number = 2;

    constructor() { }

    ngOnInit() { }

    /** Role tags **/

    /**
     * 
     * @param roles (Array<string>)
     * @param toIgnore (string)
     */
    prepRoleTags(roles: Array<string>, toIgnore: string) {
        console.log("---- this.item ----");
        console.log(this.item);
        let ignoreTag = "Starbase";
        let temp: Array<string> = [];

        for (let i = 0; i < roles.length; i++) {
            if (roles.indexOf(toIgnore) != i) {
                temp.push(roles[i]);
            }
        }

        temp.splice(temp.indexOf(ignoreTag), 1);

        let result = { main: [], extras: [] };

        for (let i = 0; i < temp.length; i++) {
            if (i < this.limitTags) {
                result.main.push(temp[i]);
            }
            if (i == this.limitTags) {
                result.main.push("+" + (temp.length - this.limitTags));
                result.extras.push(temp[i])
            }
            if (i > this.limitTags) {
                result.extras.push(temp[i]);
            }
        }
        
        return result;
    }

}
