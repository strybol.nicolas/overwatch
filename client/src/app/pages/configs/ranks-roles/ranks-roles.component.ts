import { Component, OnInit } from '@angular/core';

import { Configs } from '../../../core/models/configs/configs';

import { ConfigsService } from '../../../core/services';

@Component({
    selector: 'app-ranks-roles',
    templateUrl: './ranks-roles.component.html',
    styleUrls: ['./ranks-roles.component.scss']
})
export class RanksRolesComponent implements OnInit {

    configs: Configs;

    ranksData;
    rolesData;

    constructor(
        private _configsService: ConfigsService
    ) { }

    ngOnInit() { this.init(); }

    /**
     * Calling API to retrieve configs & preparing to send ranks & roles to children
     */
    init() {
        console.log("---- this._configsService.configs$ ----");
        this._configsService.getAll().subscribe(data => {
            this.configs = data[0];

            this.ranksData = this.configs.ranks;
            console.log("this.ranksData");
            console.log(this.ranksData);
            this.rolesData = this.configs.roles;
            console.log("this.rolesData");
            console.log(this.rolesData);
        });
    }

    /**
     * Processes EventEmitter to properly update ranks, from configs, to the API
     * @param ranks 
     */
    onRanksChange(ranks: any) {
        for( let i = 0; i < ranks.length; i++ ) {
            this.configs.ranks.starbase[ranks[i].category] = ranks[i].list;
        }

        this._configsService.update(this.configs);
    }

    /**
     * Processes EventEmitter to properly update roles, from configs, to the API
     * @param roles 
     */
    onRolesChange(roles: any) {
        for( let i = 0; i < roles.length; i++ ) {
            this.configs.roles.starbase[roles[i].category] = roles[i].list;
        }

        this._configsService.update(this.configs);
    }

}
