import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { moveItemInArray, CdkDragDrop } from '@angular/cdk/drag-drop';

@Component({
    selector: 'app-table',
    templateUrl: './table.component.html',
    styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

    @Output() onSaveChange = new EventEmitter<object>();
    
    @Input() 
    set tableData(received) {
        console.log("-- received --");

        this.key = received.key;
        this.data = received.children;
        this.length = received.children.length;
        console.log(this.data);
        console.log(this.key);
        console.log(this.length);
    };

    data;

    length;
    key: string = "";

    constructor() { }

    ngOnInit() { }

    /**
     * Increase this.data.list size by 1, by adding an empty string to array
     * Recommenced way of adding element to array/list
     */
    insert(): void {
        this.data = [ 
            ...this.data, 
            {
                title: "",
                key: this.setKey(this.data.length),
                isLeaf: true
            } 
        ];
    }

    setKey(length) {
        let endNote = (length < 10) ? "0" + length : length;

        this.length++;
        return this.key + "-" + endNote;
    }

    /**
     * Filter out element, with param as index, from this.data.list
     * Recommenced way of removing element to array/list
     * @param index 
     */
    delete(index: number): void {
        this.data = this.data.filter( (el, i) => i !== index );
    }

    /**
     * Emit this.data, and any containing changes, to calling parent 
     */
    save(): void {
        console.log(this.data);
        console.log(this.length);
        //this.onSaveChange.emit(this.data);
    }

    /** nzPopconfirm  Methods**/
    
    /**
     * Executes on clicking the "confirmation" button
     */
    onConfirm(index: number): void { this.delete(index); }

    /**
     * Executes on clicking the "cancel" button
     */
    onCancel(): void { }

    /**
     * Allows table row to be moved, automatically sets new idnexes to the respective elements
     * @param event 
     */
    drop(event: CdkDragDrop<string[]>): void {
        moveItemInArray(this.data, event.previousIndex, event.currentIndex);
    }

    /**
     * Tracks element of this.data.list in *ngFor
     * @param index 
     */
    trackBy(index) {
        return index;
    }
}
