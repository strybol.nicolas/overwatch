import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-card',
    templateUrl: './card.component.html',
    styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
    
    @Output() onRanksChange = new EventEmitter<object>();
    @Output() onRolesChange = new EventEmitter<object>();

    @Input() 
    set setRanks(received) {
        this.title = "Ranks";
        this.init(received);
    }
    @Input()
    set setRoles(received) {
        this.title = "Roles";
        this.init(received);
    }

    listOfData;

    title: string;              // Used by nzTitle attribute of nz-card

    loading: boolean = true;

    constructor() { }

    ngOnInit() { }

    /**
     * Initialization  of Input() variable
     */
    init(data) {
        this.listOfData = data[0].children;
        console.log("---- init() card ----");
        console.log(this.listOfData);
        this.loading = false;      
    }

    /**
     * Will take property name and format it to be  mkroe suitable for users' eyes
     * @param title (string)
     */
    getFormattedTitle(title: string): string {
        let result: string = "";
        let temp = title.split(/(?=[A-Z])/);
        
        result += temp[0][0].toUpperCase() + temp[0].substr(1);
        result += (temp.length == 2) ? (" " + temp[1]) : "";

        return result;
    }

    /**
     * On receiving EventEmitter, calls this.handleChanges() to correctly emit this.listOfData
     * @param result (object)
     */
    onSaveChange(result: object) { this.handleChanges(); }

    /**
     *  Emit this.listOfData, depending on this.title, to correct parent EventEmitter processor
     */
    handleChanges() {

        switch( this.title ) {
            case "Ranks":
                this.onRanksChange.emit(this.listOfData);
                break;
            case "Roles":
                this.onRolesChange.emit(this.listOfData);
                break;
            default:
                console.log("Could not save Ranks/Roles!!!");
                break;
        }
    }
    
}
