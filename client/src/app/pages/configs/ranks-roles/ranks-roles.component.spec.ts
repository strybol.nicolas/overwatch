import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RanksRolesComponent } from './ranks-roles.component';

describe('RanksRolesComponent', () => {
  let component: RanksRolesComponent;
  let fixture: ComponentFixture<RanksRolesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RanksRolesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RanksRolesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
