import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/shared.module';

import { DragDropModule } from '@angular/cdk/drag-drop';

import { 
    CardComponent,
    TableComponent 
} from './components';

const COMPONENTS = [
    CardComponent,
    TableComponent
];

@NgModule({
    declarations: [
        ...COMPONENTS,
    ],
    imports: [
        CommonModule,
        SharedModule,
        DragDropModule
    ],
    exports: [
        ...COMPONENTS
    ],
})
export class RanksRolesModule {}