import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

    configs: Array<any>;

    nzOptions = [
        {
            value: 'swr',
            label: 'Steelwing Ravens',
            children: [
                {
                    value: 'leader',
                    label: 'Leader',
                    isLeaf: true
                },
                {
                    value: 'general',
                    label: 'Ningbo',
                    isLeaf: true
                }
            ]
        },
        {
            value: 'wi',
            label: 'Weyland Industries',
            children: [
                {
                    value: 'director',
                    label: 'Director',
                    isLeaf: true
                },
                {
                    value: 'clerk',
                    label: 'Clerk',
                    isLeaf: true
                }
            ]
        }
    ];

    otherOptions = [
        {
            value: 'fujian',
            label: 'Fujian',
            children: [
                {
                    value: 'xiamen',
                    label: 'Xiamen',
                    children: [
                        {
                            value: 'Kulangsu',
                            label: 'Kulangsu',
                            isLeaf: true
                        }
                    ]
                }
            ]
        },
        {
            value: 'guangxi',
            label: 'Guangxi',
            children: [
                {
                    value: 'guilin',
                    label: 'Guilin',
                    children: [
                        {
                            value: 'Lijiang',
                            label: 'Li Jiang River',
                            isLeaf: true
                        }
                    ]
                }
            ]
        }
    ];

    constructor() { }

    values: any[] | null = null;

    ngOnInit(): void {
    }


    onChanges(values: any): void {
        console.log(this.values);
    }

}
