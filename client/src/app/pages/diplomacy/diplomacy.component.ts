import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-diplomacy',
    templateUrl: './diplomacy.component.html',
    styleUrls: ['./diplomacy.component.scss']
})
export class DiplomacyComponent implements OnInit {

    diplomacyList = {
        allies: [],
        enemies: [1, 2, 3],
        neutrals: [],
        naps: [],
    };

    constructor() { }

    ngOnInit() {
    }

    init() {


    }
}
