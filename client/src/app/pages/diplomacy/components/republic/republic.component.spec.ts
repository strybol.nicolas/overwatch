import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RepublicComponent } from './republic.component';

describe('RepublicComponent', () => {
  let component: RepublicComponent;
  let fixture: ComponentFixture<RepublicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RepublicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RepublicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
