import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-diplomacy-enemies',
    templateUrl: './enemies.component.html',
    styleUrls: ['./enemies.component.scss']
})
export class EnemiesComponent implements OnInit {

    @Input() enemies: Array<Object> = [];

    constructor() { }

    ngOnInit() {
    }

    init() {
        
    }
}
