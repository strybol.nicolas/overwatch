import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NeutralsComponent } from './neutrals.component';

describe('NeutralsComponent', () => {
  let component: NeutralsComponent;
  let fixture: ComponentFixture<NeutralsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NeutralsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NeutralsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
