import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../shared/shared.module';

import { 
    AlliesComponent,
    EnemiesComponent,
    NapsComponent,
    NeutralsComponent,
    RepublicComponent
 } from './components';


const COMPONENTS = [
    RepublicComponent,
    AlliesComponent,
    EnemiesComponent,
    NeutralsComponent,
    NapsComponent
];

@NgModule({
    declarations: [
        ...COMPONENTS
    ],
    imports: [
        CommonModule,
        SharedModule
    ],
    exports: [
        ...COMPONENTS
    ],
})
export class DiplomacyModule {}