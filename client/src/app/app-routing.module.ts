
import { NgModule } from '@angular/core';

import { 
    Routes, 
    RouterModule 
} from '@angular/router';

import {
	HomeComponent,
    AboutUsComponent,
    DiplomacyComponent,

    StarbaseComponent,
    OtherComponent,
    
    RanksRolesComponent,
	
	UserProfileComponent,
    SettingsComponent,

    PageNotFoundComponent,
} from './pages'


const routes: Routes = [
	// Sidebar Routes
	{ path: 'home', component: HomeComponent },
    { path: 'about-us', component: AboutUsComponent },
    
    { path: 'members/starbase', component: StarbaseComponent },
    { path: 'members/other', component: OtherComponent },
    
    { path: 'diplomacy', component: DiplomacyComponent },
    { path: 'configs/ranks_roles', component: RanksRolesComponent },

	// Dropdown Routes
	{ path: 'profile', component: UserProfileComponent },
	{ path: 'settings', component: SettingsComponent },

	// Misc Routes
	{ path: '', redirectTo: '/home', pathMatch: 'full' },
	{ path: '**', component: PageNotFoundComponent }
];

@NgModule({
	imports: [
		RouterModule.forRoot(routes)
	],
	exports: [RouterModule]
})
export class AppRoutingModule { }
