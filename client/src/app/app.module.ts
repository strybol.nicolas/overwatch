
import { NgModule } from '@angular/core';

import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { IconsProviderModule } from './icons-provider.module';
import { NZ_I18N, en_US } from 'ng-zorro-antd';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';

import { AppRoutingModule } from './app-routing.module';

/** Modules **/
import { SharedModule } from './shared/shared.module';
import { HeaderModule } from './shared/header/header.module';

import { StarbaseModule } from './pages/members/starbase/starbase.module';
import { OtherModule } from './pages/members/other/other.module';

import { DiplomacyModule } from './pages/diplomacy/diplomacy.module';
import { RanksRolesModule } from './pages/configs/ranks-roles/ranks-roles.module';

const MODULES = [
    SharedModule,
    HeaderModule,

    StarbaseModule,
    OtherModule,

    DiplomacyModule,

    RanksRolesModule
];

/** Services **/
import { ConfigsService } from '@core/services';

/** App Component **/
import { AppComponent } from './app.component';

/** Shared Component **/
import { 
	HeaderComponent,
	SidebarComponent
} from './shared';

/**  **/
import { 
    HomeComponent,
    AboutUsComponent,

    StarbaseComponent,
    OtherComponent,

    DiplomacyComponent,

    RanksRolesComponent,

	UserProfileComponent,
    SettingsComponent,

    PageNotFoundComponent
} from './pages';

const SIDEBARCOMPONENTS = [
    HomeComponent,
    AboutUsComponent,

    StarbaseComponent,
    OtherComponent,

    DiplomacyComponent,

    RanksRolesComponent,
];

const DROPDOWNCOMPONENTS = [
    UserProfileComponent,
    SettingsComponent,
];

registerLocaleData(en);

@NgModule({
	declarations: [
		AppComponent,
		/** Shared Components **/
		HeaderComponent,
		SidebarComponent,
		//FooterComponent,
		/** Pages Components **/
        SIDEBARCOMPONENTS,          // Sidebar Components
        DROPDOWNCOMPONENTS,         // Dropwdown Components
		PageNotFoundComponent,      // Misc. Components     
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		IconsProviderModule,
		FormsModule,
		HttpClientModule,
		BrowserAnimationsModule,
		/* Modules */
        ...MODULES
	],
	providers: [
        { provide: NZ_I18N, useValue: en_US }
    ],
	bootstrap: [AppComponent]
})
export class AppModule {}
