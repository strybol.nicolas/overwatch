import { Component } from '@angular/core';

/** NzIconService & Icons **/
import { NzIconService } from 'ng-zorro-antd';
import { ICONS } from 'src/mdi-icons';

/** Services **/
import { ConfigsService } from './core/services/';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    providers: [NzIconService]
})
export class AppComponent {
	isCollapsed = false;

	configs: any;

	constructor(
        private _iconService: NzIconService,
        private _configsService: ConfigsService
	) {
        this._iconService.addIcon(...ICONS);	// Loading mdi Icons

        this._configsService.getAll();
    }

	ngOnInit() { }
}
