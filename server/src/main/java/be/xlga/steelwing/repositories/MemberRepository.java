package be.xlga.steelwing.repositories;


import be.xlga.steelwing.models.Member;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin(origins = "http://localhost:4200")
public interface MemberRepository extends MongoRepository<Member, ObjectId> { }