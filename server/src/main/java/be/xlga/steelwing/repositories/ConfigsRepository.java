package be.xlga.steelwing.repositories;

import be.xlga.steelwing.models.Configs;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import org.springframework.web.bind.annotation.CrossOrigin;


@CrossOrigin(origins = "http://localhost:4200")
public interface ConfigsRepository extends MongoRepository<Configs, ObjectId> { }
