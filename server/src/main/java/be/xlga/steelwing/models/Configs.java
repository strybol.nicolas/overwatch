package be.xlga.steelwing.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "configs")
public class Configs {

    @Id
    private String id;
    private Object ranks;
    private Object roles;

    /** toString() **/
    @Override
    public String toString() {

        return "Configs: (id=" + id + ", [ " + ranks.toString() + " ; " + roles.toString() + " ])";
    }

    /** Setters **/

    /**
     * Replaces current 'id' with the receiving 'id'
     * @param id (String)
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Replaces current 'ranks' with the receiving 'ranks'
     * @param ranks (List<Object>)
     */
    public void setRanks(Object ranks) {
        this.ranks = ranks;
    }

    /**
     * Replaces current 'ranks' with the receiving 'ranks'
     * @param roles (List<Object>)
     */
    public void setRoles(Object roles) {
        this.roles = roles;
    }

    /** Getters **/

    /**
     * Sends contents of 'id' to calling method
     * @return id (String)
     */
    public String getId() {
        return id;
    }

    /**
     * Sends contents of 'ranks' to calling method
     * @return ranks (List<Object>)
     */
    public Object getRanks() {
        return ranks;
    }

    /**
     * Sends contents of 'roles' to calling method
     * @return roles (List<Object>)
     */
    public Object getRoles() {
        return roles;
    }

    /*public class Order {

        private String category;
        private List<String> data;

        @Override
        public String toString() {

            String result ="";

            result += "{ " + category + ", " + data.toString() + " }";

            return result;
        }

        /** Setters **/


        /*public void setCategory(String category) {
            this.category = category;
        }*/


        /*public void setData(List<String> data) {
            this.data = data;
        }*/


        /** Getters **/


        /*public String getCategory() {
            return category;
        }*/


        /*public List<String> getData() {
            return data;
        }*/

    /*}*/
}
