package be.xlga.steelwing.models;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "members")
public class Member {

    @Id
    private String id;

    private Discord discordInfo;
    private Object ranks;
    private Object roles;

    public Member() { }

    public String toString() {
        String result = "";

        result += "Member: (id=" + id + ", { " + discordInfo.toString() + " }, { [ " + ranks + " ] }, { [ " + roles + " ] } )";

        return result;
    }

    /** Setters **/
    public void setId(String id) {
        this.id = id;
    }

    public void setDiscordInfo(Discord discordInfo) {
        this.discordInfo = discordInfo;
    }

    public void setRanks(Object ranks) {
        this.ranks = ranks;
    }

    public void setRoles(Object roles) {
        this.roles = roles;
    }


    /** Getters **/
    public String getId() {
        return id;
    }

    public Object getRanks() {
        return ranks;
    }

    public Object getRoles() {
        return roles;
    }

    public Discord getDiscordInfo() {
        return discordInfo;
    }


    /** Discord Class **/
    public class Discord {

        @Indexed(unique = true)
        private String id;
        private String username;
        private String tag;

        /** toString() **/
        @Override
        public String toString() {

            return "Discord: " + username + "#" + tag + " ( " + id + " )";
        }


        /** Setters **/

        /**
         * Replaces current 'discordId' with the receiving 'discordId'
         * @param id (String)
         */
        public void setId(String id) {
            this.id = id;
        }

        /**
         * Replaces current 'username' with the receiving 'username'
         * @param username (String)
         */
        public void setUsername(String username) {
            this.username = username;
        }

        /**
         * Replaces current 'tag' with the receiving 'tag'
         * @param tag (int)
         */
        public void setTag(String tag) {
            this.tag = tag;
        }


        /** Getters **/

        /**
         * Replaces current 'discordId' with the receiving 'discordId'
         * @return id (String)
         */
        public String getId() {
            return id;
        }

        /**
         * Replaces current 'username' with the receiving 'username'
         * @return username (String)
         */
        public String getUsername() {
            return username;
        }

        /**
         * Replaces current 'tag' with the receiving 'tag'
         * @return tag (int)
         */
        public String getTag() {
            return tag;
        }
    }
}
