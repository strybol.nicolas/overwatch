package be.xlga.steelwing.controllers;


import org.bson.types.ObjectId;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import be.xlga.steelwing.models.Configs;
import be.xlga.steelwing.repositories.ConfigsRepository;

@RestController
@RequestMapping(value="/api/configs")
class ConfigsController {
    private ConfigsRepository repository;

    public ConfigsController(ConfigsRepository repository) {
        this.repository = repository;
    }

    @GetMapping
    public List<Configs> getAll() {
        System.out.println("---- getAll (Configs) ----");
        List<Configs> result = repository.findAll();
        System.out.println(result);
        return result;
    }

    @PutMapping("{id}")
    public Configs update(@PathVariable("id") ObjectId id, @RequestBody Configs configs) {
        System.out.println("---- update (Configs) ----");
        return repository.save(configs);
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public class ResourceNotFoundException extends RuntimeException {
        public ResourceNotFoundException() { }
    }
}