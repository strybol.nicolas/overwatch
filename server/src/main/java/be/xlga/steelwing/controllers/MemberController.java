package be.xlga.steelwing.controllers;

import org.bson.types.ObjectId;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import be.xlga.steelwing.models.Member;
import be.xlga.steelwing.repositories.MemberRepository;

@RestController
@RequestMapping(value="/api/members")
public class MemberController {

    private MemberRepository repository;

    public MemberController(MemberRepository repository) {
        this.repository = repository;
    }

    @GetMapping
    public List<Member> getAll() {
        System.out.println("---- getAll (Member) ----");
        repository.findAll().forEach(
                el -> {
                    System.out.println(el.toString());
                }
        );
        return repository.findAll();
    }

    @PostMapping
    public Member insert(@RequestBody Member member) {
        System.out.println("---- insert (Member) ----");
        System.out.println(member);
        return repository.insert(member);
    }

    @PutMapping("/{id}")
    public Member update(@PathVariable("id") String id, @RequestBody Member member) {
        System.out.println("---- update (Member) ----");
        return repository.save(member);
    }

    @DeleteMapping("/{id}")
    public Boolean delete(@PathVariable("id") String id) {
        System.out.println("---- delete (Member) ----");
        repository.deleteById(new ObjectId(id));
        System.out.println(repository.findById(new ObjectId(id)).isPresent());
        return repository.findById(new ObjectId(id)).isPresent();
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public class ResourceNotFoundException extends RuntimeException {
        public ResourceNotFoundException() {
        }
    }
}
