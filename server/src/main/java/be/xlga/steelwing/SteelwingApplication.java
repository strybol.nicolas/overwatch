package be.xlga.steelwing;

import be.xlga.steelwing.models.Configs;
import be.xlga.steelwing.repositories.ConfigsRepository;
import be.xlga.steelwing.repositories.MemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;


import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.Ordered;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import java.util.Collections;
import java.util.List;


@SpringBootApplication
//@ComponentScan(basePackages= {"be.xlga.steelwing"})
public class SteelwingApplication implements CommandLineRunner {

    @Autowired
    private ConfigsRepository configs;
    private MemberRepository members;

    public static void main(String[] args) {
        SpringApplication.run(SteelwingApplication.class, args);

    }


    @Override
    public void run(String... args) throws Exception {


    }

    /*@Bean
    ApplicationRunner init() {
        return args -> {

            this.repository.findAll().forEach( el -> System.out.println(el.toString()));
            Configs configs = new Configs();

            configs.setRanks("Leaders, Generals");

            configs.setRoles("Squad Leader, Minsiter");

            //repository.save(configs);

            //repository.findAll().forEach(System.out::println);
        };
    }*/

    @Bean
    public FilterRegistrationBean<CorsFilter> simpleCorsFilter() {

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();

        config.setAllowCredentials(true);
        config.setAllowedOrigins(Collections.singletonList("http://localhost:4200"));
        config.setAllowedMethods(Collections.singletonList("*"));
        config.setAllowedHeaders(Collections.singletonList("*"));

        source.registerCorsConfiguration("/**", config);

        FilterRegistrationBean<CorsFilter> bean = new FilterRegistrationBean<>(new CorsFilter(source));
        bean.setOrder(Ordered.HIGHEST_PRECEDENCE);

        return bean;
    }

}
